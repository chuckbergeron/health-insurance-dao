const deployTargetAndDelegate = require('./support/deployTargetAndDelegate')
const PatientPolicy = artifacts.require("./PatientPolicy.sol")

module.exports = function(deployer, networkName) {
  return deployTargetAndDelegate(artifacts, deployer, PatientPolicy, networkName)
};
