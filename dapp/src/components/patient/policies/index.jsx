import React, { Component } from 'react'
import classnames from 'classnames'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import FlipMove from 'react-flip-move'
import { all } from 'redux-saga/effects'
import { formatRoute } from 'react-router-named-routes'
import {
  LinkContainer
} from 'react-router-bootstrap'
import {
  cacheCall,
  contractByName,
  withSaga,
  cacheCallValue,
  cacheCallValueInt
} from '~/saga-genesis'
import { LoadingLines } from '~/components/LoadingLines'
import { ScrollToTop } from '~/components/ScrollToTop'
import { defined } from '~/utils/defined'
import range from 'lodash.range'
import get from 'lodash.get'
import * as routes from '~/config/routes'
import { fixAddress } from '~/utils/fixAddress'
import { Button } from 'react-bootstrap'

function mapStateToProps(state, props) {
  const address = get(state, 'sagaGenesis.accounts[0]')
  const PolicyManager = contractByName(state, 'PolicyManager')
  const hasPolicy = cacheCallValue(state, PolicyManager, 'patientPolicies', address)

  return {
    address,
    hasPolicy,
    PolicyManager
  }
}

function* saga({ address, PolicyManager }) {
  if (!address || !PolicyManager) { return }

  yield cacheCall(PolicyManager, 'patientPolicies', address)
}

export const PatientPolicies = connect(mapStateToProps)(withSaga(saga)(class _PatientPolicies extends Component {

  render() {
    let policy
    if (!this.props.hasPolicy) {
      policy = (
        <div className="blank-state">
          <div className="blank-state--inner text-center">
            <span>You do not have a policy.</span>

            <LinkContainer to={routes.PATIENT_POLICIES_NEW}>
              <Button className="btn-primary">
                New Policy
              </Button>
            </LinkContainer>
          </div>

        </div>
      )
    } else {
      policy = (
        <div>
          Your Policy is: hello!
        </div>
      )
    }

    return (
      <div className='container'>
        <div className='row'>
          <div className='col-xs-12 col-md-10 col-md-offset-1'>
            <div className="card">
              <ScrollToTop />
              <div className="card-body">
                {policy}
              </div>
            </div>
          </div>
        </div>
      </div>

    )
  }
}))

export const PatientPoliciesContainer = withRouter(PatientPolicies)
