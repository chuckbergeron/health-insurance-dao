import ReactDOMServer from 'react-dom/server'
import React, { Component } from 'react'
import ReactTimeout from 'react-timeout'
import Select from 'react-select'
import { connect } from 'react-redux'
import * as Animated from 'react-select/lib/animated';
import { withRouter } from 'react-router-dom'
import { Checkbox } from 'react-bootstrap'
import FlipMove from 'react-flip-move'
import PropTypes from 'prop-types'
import BN from 'bn.js'
import getWeb3 from '~/get-web3'
import { isBlank } from '~/utils/isBlank'
import { all } from 'redux-saga/effects'
import {
  contractByName,
  TransactionStateHandler,
  withSaga,
  withSend,
  cacheCall,
  cacheCallValue
} from '~/saga-genesis'
import { customStyles } from '~/config/react-select-custom-styles'
import { HEXStringDisplay } from '~/components/HEXStringDisplay'
import { HEXTextArea } from '~/components/forms/HEXTextArea'
import { Loading } from '~/components/Loading'
import { toastr } from '~/toastr'
import { etherToWei } from '~/utils/etherToWei'
import pull from 'lodash.pull'
import * as routes from '~/config/routes'
import { Button } from 'react-bootstrap'
import get from 'lodash.get'

import patientPolicyContractConfig from '#/PatientPolicy.json'
import { abiFactory } from '~/saga-genesis/utils'


function mapStateToProps(state, ownProps) {
  const address = get(state, 'sagaGenesis.accounts[0]')
  const PatientPolicy = contractByName(state, 'PatientPolicy')
  const hasPolicy = cacheCallValue(state, PatientPolicy, 'patientPolicies', address)

  // console.log('wtf')

  return {
    address,
    PatientPolicy,
    hasPolicy,
    transactions: state.sagaGenesis.transactions
  }
}


function* policySaga({ address, PatientPolicy }) {
  if (!address || !PatientPolicy) { return }

  yield cacheCall(PatientPolicy, 'patientPolicies', address)
}

export const NewPolicy = withSaga(policySaga)(withRouter(ReactTimeout(connect(mapStateToProps)(withSend(class _NewPolicy extends Component {

  constructor(props, context) {
    super(props, context)

    this.state = {
      claim: null,
      isSubmitting: false,
      errors: [],
      success: false
    }
  }

  // componentDidMount() {
  //   this.init(this.props)
  // }

  // init = (nextProps) => {
  //   if (nextProps.PatientPolicy != undefined) {
  //     this.getPolicyState(nextProps.PatientPolicy)
  //   }
  // }

  componentWillReceiveProps(nextProps) {
    // this.init(nextProps)


    const transaction = nextProps.transactions[this.state.transactionId]
    if (this.state.transactionHandler) {
      this.state.transactionHandler.handle(transaction)
        .onError((error) => {
          toastr.transactionError(error)
          this.setState({
            transactionHandler: null,
            isSubmitting: false
          })
        })
        .onReceipt(() => {
          this.setState({
            transactionHandler: null,
            isSubmitting: false
          })

          // this.getPolicyState(this.props.PatientPolicy)
        })
        .onTxHash(() => {
          this.setState({
            transactionHandler: null,
            isSubmitting: false,
            purchasedPolicy: 'B'
          })
          toastr.success("Congratulations - your policy has been purchased! Thank you for the opportunity to serve you.")
          // this.nextProps.setTimeout(() => {
            // this.nextProps.history.push(routes.DOCTOR_CLAIMS)
          // }, 1000)
        })
    }
  }

  handlePolicyAClick = () => {
    const transactionId = this.props.send(
      this.props.PatientPolicy,
      'addPatientPolicy',
      'Jane Patient',
      'A'
    )
    ({
      value: new BN(etherToWei('15')),
      from: this.props.address
    })

    this.setState({
      transactionId,
      transactionHandler: new TransactionStateHandler()
    })
  }

  handlePolicyBClick = () => {
    const transactionId = this.props.send(
      this.props.PatientPolicy,
      'addPatientPolicy',
      'Jane Patient',
      'B'
    )
    ({
      value: new BN(etherToWei('20')),
      from: this.props.address
    })

    this.setState({
      transactionId,
      transactionHandler: new TransactionStateHandler()
    })
  }

  // getPolicyState = async (PatientPolicy) => {
  //   const web3 = getWeb3()
  //   const instance = new web3.eth.Contract(patientPolicyContractConfig.abi, PatientPolicy)
  //   console.log('getPolicyState')
  //   const purchasedPolicy = await instance.methods.patientPolicies(this.props.address).call()
  //   console.log('purchasedPolicy', purchasedPolicy)
  //   this.setState({
  //     purchasedPolicy
  //   })
  // }

  render() {
    let policy

    if (this.state.purchasedPolicy) {
      policy = (
        <div className="card-header">
          <div className="row">
            <div className="col-md-8">
              <br />
              <br />
              <h3 className="card-title">
                You Purchased Policy {this.state.purchasedPolicy} !
              </h3>
              <hr />
              <br />
              <h4>
                You have joined 23,489 others who rest assured knowing they will be covered if the need arises for medical attention.
              </h4>
              <hr />
              <br />
              <br />
              <p>
                Thank you for trusting OpenCoverage.
              </p>
            </div>
          </div>
        </div>
      )
    } else {
      policy = (
        <React.Fragment>
          <div id="submit-policy" className="card-header">
            <h3 className="card-title">
              Welcome Jane Patient!
            </h3>
          </div>

          <div className="card-body">
            <p>
              Get covered. Feel better. Sign up to get health insurance for you and your family.
            </p>
            <hr />
            <Button
              onClick={this.handlePolicyAClick}
              className="btn-success col-xs-12 col-sm-4 col-sm-offset-1"
            >
              <h3>Policy A</h3>
              <p>15 ETH</p>
              <p>Minimum Coverage</p>
              <br />
            </Button> &nbsp;

            <Button
              onClick={this.handlePolicyBClick}
              className="btn-success col-xs-12 col-sm-4 col-sm-offset-2"
            >
              <h3>Policy B</h3>
              <p>20 ETH</p>
              <p>Maximum Coverage</p>
              <br />
            </Button>
          </div>
        </React.Fragment>
      )
    }

    let content = (
      <div className="row">
        <div className="col-xs-6 col-md-6">
          <h3>Policy A</h3>
          <p>Cost: 0.1 ETH</p>
          <Button onClick={this.handlePolicyAClick}>Policy 1</Button>
        </div>

        <div className="col-xs-6 col-md-6">
          <h3>Policy B</h3>
          <p>Cost: 0.2 ETH</p>
          <Button onClick={this.handlePolicyBClick}>Policy 2</Button>
        </div>
      </div>
    )

    if (this.state.success) {
      content = (
        <div className="row">
          <div className="col-xs-12 col-md-12">
            <img src="/success.png" alt="" style={{float: 'right', width: 300}}/>
            <h1>Success!</h1>
            <p>We have received your payment and your policy has been created.</p>
          </div>
        </div>
      )
    }

    return (
      <div>
        <div className="container">
          <div className="row">
            <div className='col-xs-12 col-md-10 col-md-offset-1'>
              <div className="card">
                {policy}
              </div>
            </div>
          </div>
        </div>

        <Loading loading={this.state.isSubmitting} />
      </div>
    )
  }
})))))
