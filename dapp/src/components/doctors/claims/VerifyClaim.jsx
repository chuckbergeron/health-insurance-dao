import ReactDOMServer from 'react-dom/server'
import React, { Component } from 'react'
import ReactTimeout from 'react-timeout'
import classnames from 'classnames'
import Select from 'react-select'
import { connect } from 'react-redux'
import * as Animated from 'react-select/lib/animated';
import { withRouter } from 'react-router-dom'
import { Checkbox } from 'react-bootstrap'
import FlipMove from 'react-flip-move'
import PropTypes from 'prop-types'
import { isBlank } from '~/utils/isBlank'
import {
  contractByName,
  TransactionStateHandler,
  withSend
} from '~/saga-genesis'
import { customStyles } from '~/config/react-select-custom-styles'
import { HEXStringDisplay } from '~/components/HEXStringDisplay'
import { HEXTextArea } from '~/components/forms/HEXTextArea'
import { HEXToggleButtonGroup } from '~/components/forms/HEXToggleButtonGroup'
import { Loading } from '~/components/Loading'
import { toastr } from '~/toastr'
import pull from 'lodash.pull'
import * as routes from '~/config/routes'

function mapStateToProps (state, ownProps) {
  const DoctorClaims = contractByName(state, 'DoctorClaims')

  return {
    DoctorClaims,
    transactions: state.sagaGenesis.transactions
  }
}

export const VerifyClaimContainer = withRouter(ReactTimeout(connect(mapStateToProps)(withSend(class _VerifyClaimContainer extends Component {
  static propTypes = {
    claimAddress: PropTypes.string,
    claimHash: PropTypes.string
  }

  constructor(props, context){
    super(props, context)

    this.state = {
      isSubmitting: false,
      errors: [],
      approveReject: null
    }
  }

  componentWillReceiveProps(props) {
    const transaction = props.transactions[this.state.transactionId]
    if (this.state.transactionHandler) {
      this.state.transactionHandler.handle(transaction)
        .onError((error) => {
          toastr.transactionError(error)
          this.setState({
            transactionHandler: null,
            isSubmitting: false
          })
        })
        .onReceipt(() => {
          this.setState({
            transactionHandler: null,
            isSubmitting: false
          })
        })
        .onTxHash(() => {
          this.setState({
            transactionHandler: null,
            isSubmitting: false
          })
          toastr.success("Thank You for verifying this claim.")
          this.props.setTimeout(() => {
            this.props.history.push(routes.DOCTOR_CLAIMS)
          }, 1000)
        })
    }
  }

  handleSubmit = async (event) => {
    event.preventDefault()
    let transactionId

    if (this.state.approveReject === 'Approve') {
      transactionId = this.props.send(
        this.props.DoctorClaims,
        'approveClaim'
      )
      ({
        from: this.props.address
      })
    } else if (this.state.approveReject === 'Reject') {
      transactionId = this.props.send(
        this.props.DoctorClaims,
        'denyClaim'
      )
      ({
        from: this.props.address
      })
    }

    this.setState({
      transactionId,
      transactionHandler: new TransactionStateHandler()
    })
  }

  handleButtonGroupOnChange = (event) => {
    this.setState({ [event.target.name]: event.target.value })
  }

  render() {
    const loading = this.state.isSubmitting

    let errors = {}
    for (var i = 0; i < this.state.errors.length; i++) {
      let fieldName = this.state.errors[i]

      errors[fieldName] =
        <p key={`errors-${i}`} className='has-error help-block small'>
          {this.errorMessage(fieldName)}
        </p>
    }

    return (
      <div className='container'>
        <div className="row">
          <div className="col-xs-12 col-md-8 col-md-offset-2">
            <div className="card">
              <form onSubmit={this.handleSubmit} >
                <div id="submit-claim" className="card-header">
                  <h3 className="card-title">
                    Approve or Deny Claim
                  </h3>
                </div>
                <div className="card-body">
                  <div className="row">
                    <div className="col-xs-12 col-md-4">
                      <label>
                        Patient:
                      </label>
                      <p>
                        Jane Patient
                      </p>
                    </div>
                    <div className="col-xs-12 col-md-4">
                      <label>
                        Treatment:
                      </label>
                      <p>
                        Appendicitis
                      </p>
                    </div>
                    <div className="col-xs-12 col-md-4">
                      <label>
                        Treating Doctor:
                      </label>
                      <p>
                        Dr. OpenCoverage 1
                      </p>
                    </div>
                  </div>
                  <br />

                  <div className="row">
                    <div className="col-xs-12 col-md-8">
                      <label>
                        Hospital:
                      </label>
                      <p>
                        Zuck's Zuckerberg General Hospital
                      </p>
                    </div>
                    <div className="col-xs-12 col-md-4">
                      <label>
                        Case ID:
                      </label>
                      <p>
                        #987623487
                      </p>
                    </div>
                  </div>

                  <hr />

                  <div className="row">
                    <div className="col-xs-12 col-md-8">

                      <div className={classnames('form-group')}>
                        <HEXToggleButtonGroup
                          id='approveReject'
                          name="approveReject"
                          colClasses='col-xs-12'
                          label=''
                          buttonGroupOnChange={this.handleButtonGroupOnChange}
                          values={['Approve', 'Reject']}
                        />
                      </div>

                      <FlipMove
                        enterAnimation="accordionVertical"
                        leaveAnimation="accordionVertical"
                      >
                        {this.state.approveReject === 'Reject' ?
                          <HEXTextArea
                            key='test'
                            id='reason'
                            name='reason'
                            colClasses='col-xs-12 col-sm-12 col-md-12'
                            label='Reason'
                            optional={false}
                            textAreaOnChange={this.handleTextAreaOnChange}
                          />
                          : (
                            <div
                              key='not-test'
                            />
                          )
                        }

                      </FlipMove>


                    </div>

                  </div>
                </div>
                <div className="card-footer text-right">
                  <button
                    disabled={this.state.approveReject === null}
                    type="submit"
                    className="btn btn-lg btn-success"
                  >
                    Submit
                    {
                      this.state.approveReject !== null ?
                        this.state.approveReject === 'Approve' ? ' Approval' : ' Rejection'
                        : ''
                    }
                  </button>
                </div>
              </form>
            </div>

            <Loading loading={loading} />
          </div>
        </div>
      </div>
    )
  }
}))))
