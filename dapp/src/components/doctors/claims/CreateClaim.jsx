import ReactDOMServer from 'react-dom/server'
import React, { Component } from 'react'
import ReactTimeout from 'react-timeout'
import Select from 'react-select'
import { connect } from 'react-redux'
import * as Animated from 'react-select/lib/animated';
import { withRouter } from 'react-router-dom'
import { FormGroup, FormControl, ControlLabel, HelpBlock, Button } from 'react-bootstrap'
import FlipMove from 'react-flip-move'
import PropTypes from 'prop-types'
import { isBlank } from '~/utils/isBlank'
import {
  contractByName,
  TransactionStateHandler,
  withSend,
  cacheCall,
  cacheCallValueBigNumber,
  withSaga
} from '~/saga-genesis'
import { customStyles } from '~/config/react-select-custom-styles'
import { HEXStringDisplay } from '~/components/HEXStringDisplay'
import { HEXTextArea } from '~/components/forms/HEXTextArea'
import { Loading } from '~/components/Loading'
import { toastr } from '~/toastr'
import getWeb3 from '~/get-web3'

import pull from 'lodash.pull'
import * as routes from '~/config/routes'
import BN from 'bn.js'
import { etherToWei } from '~/utils/etherToWei'
import get from 'lodash.get'

import doctorClaimsContractConfig from '#/DoctorClaims.json'
import { abiFactory } from '~/saga-genesis/utils'

function mapStateToProps(state, ownProps) {
  const address = get(state, 'sagaGenesis.accounts[0]')
  const DoctorClaims = contractByName(state, 'DoctorClaims')
  const PatientPolicy = contractByName(state, 'PatientPolicy')
  const DoctorManager = contractByName(state, 'DoctorManager')
  let treatmentPoints

  if (address && DoctorClaims) {
    // treatmentPoints = cacheCallValueBigNumber(state, DoctorClaims, 'treatmentTokenBalances', address.toLowerCase())

    // const web3 = getWeb3()
    // const instance = new web3.eth.Contract(doctorClaimsContractConfig.abi, DoctorClaims)
    // treatmentPoints = instance.methods.treatmentTokenBalances(this.props.address).call()
    // // console.log(address.toLowerCase())
    // console.log(treatmentPoints)
  }


  // console.log('FIND THIS', PatientPolicy)
  // console.log('FIND THIS', DoctorManager)

  return {
    DoctorClaims,
    PatientPolicy,
    DoctorManager,
    treatmentPoints,
    transactions: state.sagaGenesis.transactions
  }
}

function* claimsSaga({ address, DoctorClaims }) {
  console.log(address, DoctorClaims)
  if (!address || !DoctorClaims) { return }

  yield cacheCall(DoctorClaims, 'treatmentTokenBalances', address)
}

export const CreateClaimContainer = withSaga(claimsSaga)(withRouter(ReactTimeout(connect(mapStateToProps)(withSend(class _CreateClaimContainer extends Component {

  constructor(props, context) {
    super(props, context);

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

    this.state = {
      value: '',
      success: false
    };
  }

  getValidationState() {
    const length = this.state.value;
    if (length > 0) return 'success';
    else if (length <= 0) return 'error';
    return null;
  }

  handleChange(e) {
    this.setState({ value: e.target.value });
  }

  handleSubmit(e) {
    e.preventDefault()

    const transactionId = this.props.send(
      this.props.DoctorClaims,
      'createClaim',
      '0x5a9784cbfae9dfc2a09a77a3c92a6a62156eaa12', // verifier address
      '0xab3ebaeed25a92e9cd4fb6ec37fa0043c75c6490', // patient address
      new BN(etherToWei(this.state.value)) // reimbursement amount
    )
      ({
        value: new BN(etherToWei(this.state.value * 0.02)),
        from: this.props.address
      })

      this.setState({
        transactionId,
        transactionHandler: new TransactionStateHandler()
      })
  }


  componentWillReceiveProps(props) {
    const transaction = props.transactions[this.state.transactionId]
    if (this.state.transactionHandler) {
      this.state.transactionHandler.handle(transaction)
        .onError((error) => {
          toastr.transactionError(error)
          this.setState({
            transactionHandler: null,
            isSubmitting: false
          })
        })
        .onReceipt(() => {
          this.setState({
            transactionHandler: null,
            isSubmitting: false
          })
        })
        .onTxHash(() => {
          this.setState({
            transactionHandler: null,
            isSubmitting: false,
            success: true
          })
          toastr.success("Thank You - Your claim was received successfully. A verifier will review your claim soon.")
          this.props.setTimeout(() => {
            // this.props.history.push(routes.DOCTOR_CLAIMS)
          }, 1000)
        })
    }
  }

  render() {
    if (this.state.success) {
      return (
        <div>
          <div className="container">
            <div className="row">
              <div className='col-xs-12 col-md-8'>
                <div className="card">
                  <div className="card-header">
                    <h2>Claim Submitted!</h2>
                    <h3>
                      A verifier will soon review your claim.
                    </h3>
                  </div>
                  <div className="card-body">

                  </div>
                </div>
              </div>
              <div className='col-xs-12 col-md-4'>
                Treatment Points
              </div>

            </div>
          </div>
        </div>
      )
    }

    return (
      <div>
        <div className="container">
          <div className="row">
            <div className='col-xs-12 col-md-8'>
              <div className="card">
                <div className="card-header">
                  <h2>Create New Claim</h2>
                </div>
                <div className="card-body">
                  <form onSubmit={this.handleSubmit}>
                    <FormGroup
                      controlId="formBasicText"
                    >
                      <ControlLabel>Patient:</ControlLabel>
                      <select>
                        <option value="Jane Patient">--- Choose a Patient ---</option>
                        <option value="Jane Patient">Veronica Mars</option>
                        <option value="Jane Patient">Jane Patient</option>
                        <option value="Jane Patient">Donald Glover</option>
                        <option value="Jane Patient">Peter Parker</option>
                      </select>
                    </FormGroup>

                    <FormGroup
                      controlId="formBasicText"
                    >
                      <ControlLabel>Enter amount of ETH for claim.</ControlLabel>
                      <FormControl
                        type="text"
                        value={this.state.value}
                        placeholder="ETH"
                        onChange={this.handleChange}
                      />
                      <FormControl.Feedback />
                      <HelpBlock>A refundable deposit of <strong>{this.state.value * 0.02} ETH</strong> will be deducted for this claim.</HelpBlock>
                    </FormGroup>

                    <Button type="submit" className="btn-success btn-lg">Submit Claim</Button>
                  </form>
                </div>
              </div>
            </div>

            <div className='col-xs-12 col-md-4'>
              <br />
              <br />
              <label>
                Treatment Points:
              </label>
              <p>
                {this.props.treatmentPoints ? this.props.treatmentPoints : '0'}
              </p>

            </div>
          </div>
        </div>
      </div>
    )
  }
})))))
