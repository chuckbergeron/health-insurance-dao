import React, { Component } from 'react'
import { withRouter, Route, Switch, Redirect } from 'react-router-dom'
import ReduxToastr from 'react-redux-toastr'
import ReactTimeout from 'react-timeout'
import { hot } from 'react-hot-loader'
import { formatRoute } from 'react-router-named-routes'
import { newAsyncWrap } from '~/components/newAsyncWrap'
import { NewPolicy } from '~/components/patient/policies/NewPolicy'
// import { PatientPoliciesContainer } from '~/components/patient/policies'
import { PatientPolicyContainer } from '~/components/patient/policies/PatientPolicy'
import { AdminDoctors } from '~/components/admin/AdminDoctors'
import { Welcome } from '~/components/welcome'
import { TryMetamask } from '~/components/try-metamask'
import { LoginToMetaMask } from '~/components/login-to-metamask'
import { VerifyClaimContainer } from '~/components/doctors/claims/VerifyClaim'
import { DoctorsContainer } from '~/components/doctors'
import { CreateClaimContainer } from '~/components/doctors/claims/CreateClaim'
import { FourOhFour } from '~/components/four-oh-four'
import { OpenCoverageNavbarContainer } from '~/components/navbar/OpenCoverageNavbar'
import { NetworkCheckModal } from '~/components/NetworkCheckModal'
import { isBlank } from '~/utils/isBlank'
import * as routes from '~/config/routes'
import { Web3Route } from '~/components/Web3Route'
import { DebugLog } from '~/components/DebugLog'
import { connect } from 'react-redux'
import {
  cacheCall,
  cacheCallValue,
  cacheCallValueInt,
  cacheCallValueBigNumber,
  contractByName,
  LogListener,
  withSaga
} from '~/saga-genesis'
import { toastr } from '~/toastr'
import get from 'lodash.get'
import { fixAddress } from '~/utils/fixAddress'

function mapStateToProps (state) {
  const address = get(state, 'sagaGenesis.accounts[0]')
  const networkId = get(state, 'sagaGenesis.network.networkId')

  const DoctorManager = contractByName(state, 'DoctorManager')

  const isDoctor = cacheCallValue(state, DoctorManager, 'isDoctor', address)

  // nextCaseAddress = fixAddress(cacheCallValue(state, CaseManager, 'doctorCaseAtIndex', address, (doctorCasesCount - 1)))
  const isOwner = address && (cacheCallValue(state, DoctorManager, 'owner') === address)

  return {
    address,
    networkId,
    isDoctor,
    DoctorManager,
    // nextCaseAddress,
    isOwner
  }
}

function mapDispatchToProps(dispatch) {
  return {
    dispatchSignOut: () => {
      dispatch({ type: 'SIGN_OUT' })
    }
  }
}

function* saga({ address, DoctorManager }) {
  if (!address || !DoctorManager) { return }
  const isDoctor = yield cacheCall(DoctorManager, 'isDoctor', address)

  // yield cacheCall(CaseManager, 'doctorCaseAtIndex', address, (doctorCasesCount - 1))
}

const App = ReactTimeout(connect(mapStateToProps, mapDispatchToProps)(
  withSaga(saga)(
    class _App extends Component {
      constructor(props) {
        super(props)
        this.state = {
          debugging: false
        }
      }

  componentWillReceiveProps (nextProps) {
    this.onAccountChangeSignOut(nextProps)
  }

  onAccountChangeSignOut (nextProps) {
    // Sign out the localStorage/browser session when the users Eth address changes
    const addressDoesNotMatch = this.props.address && this.props.address !== nextProps.address
    const networkDoesNotMatch = this.props.networkId && this.props.networkId !== nextProps.networkId
    if (addressDoesNotMatch || networkDoesNotMatch) {
      this.signOut()
    }
  }

  unload = () => {
    if (process.env.NODE_ENV !== 'development') {
      this.signOut()
    }
  }

  signOut () {
    this.props.dispatchSignOut()
    this.props.history.push(routes.WELCOME)
  }

  render () {
    if (this.props.isOwner) {
      var ownerWarning =
        <div className="alert alert-warning alert--banner text-center">
          <small>NOTE: You are currently using the contract owner's Ethereum address.</small>
        </div>
    }

    if (this.state.debugging) {
      var debugLog =
        <div>
          <hr />
          <DebugLog />
        </div>
    }

    var debugLink =
      <div>
        <a onClick={() => this.setState({ debugging: !this.state.debugging })} className='btn btn-info'>Toggle Log</a>
        {debugLog}
      </div>

    const WelcomeWrapped = <Welcome
      isDoctor={this.props.isDoctor}
    />

    return (
      <React.Fragment>
        <OpenCoverageNavbarContainer />
        {ownerWarning}
        <div className="content">
          <Switch>
            <Route path={routes.WELCOME} component={null} />
            <Route path='/' component={NetworkCheckModal} />
          </Switch>

          <Switch>
            <Route path={routes.WELCOME} render={ () => WelcomeWrapped } />
            <Route path={routes.LOGIN_METAMASK} component={LoginToMetaMask} />
            <Route path={routes.TRY_METAMASK} component={TryMetamask} />

            <Web3Route exact path={routes.DOCTOR_CLAIMS}
              component={DoctorsContainer}
            />
            <Web3Route exact path={routes.DOCTOR_CLAIMS_NEW}
              component={CreateClaimContainer}
            />
            <Web3Route exact path={routes.DOCTOR_CLAIMS_VERIFY}
              component={VerifyClaimContainer}
            />

            <Web3Route path={routes.ADMIN_DOCTORS} component={AdminDoctors} />

            <Web3Route exact path={routes.PATIENT_POLICIES_NEW} component={NewPolicy} />

            <Web3Route path={routes.PATIENT_POLICY} component={PatientPolicyContainer} />

            <Redirect from={routes.HOME} exact to={routes.WELCOME} />

            <Route path={routes.HOME} component={FourOhFour} />
          </Switch>
        </div>

        <footer className="footer">
          <div className='container'>
            <div className="row">
              <div className="col-sm-12 text-center">
                <p className="text-footer">
                  &copy; 2018 OpenCoverage - All Rights Reserved
                </p>
                {/*debugLink*/}
              </div>
            </div>
          </div>
        </footer>

        <ReduxToastr
          timeOut={7000}
          newestOnTop={true}
          tapToDismiss={false}
          position="bottom-left"
          transitionIn="bounceIn"
          transitionOut="bounceOut"
        />
      </React.Fragment>
  )
}
})))

export default hot(module)(withRouter(App))


//<Web3Route path={routes.PATIENT_POLICIES} component={PatientPoliciesContainer} />
