import {
  all,
  fork,
  setContext
} from 'redux-saga/effects'
import rootSagaGenesis, { takeOnceAndRun } from '~/saga-genesis/sagas'
import addTopLevelContracts from './add-top-level-contracts-saga'
import addRegistryContracts from './add-registry-contracts-saga'
import { failedTransactionListener } from './failedTransactionListener'

export default function* () {
  yield fork(takeOnceAndRun, 'WEB3_NETWORK_ID', function* ({ web3, networkId }) {
    yield setContext({ web3 })
    yield addTopLevelContracts()
    yield addRegistryContracts({ web3 })
    yield fork(all, [
      failedTransactionListener()
    ])
  })
  yield rootSagaGenesis()
}
