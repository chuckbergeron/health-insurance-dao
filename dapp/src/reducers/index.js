import { combineReducers } from 'redux'
import { sagaGenesis } from '~/saga-genesis/reducers'
import { reducer as toastr } from 'react-redux-toastr'
import keyValue from './keyValueReducer'

export default combineReducers({
  sagaGenesis,
  toastr,
  keyValue
})
